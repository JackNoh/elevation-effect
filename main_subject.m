clc;
clear all;
close all;

addpath 'CIPIC_hrtf_database/standard_hrir_database/subject_003';

%% Params
load('hrir_final.mat');

kMinEle     = -45;
kMaxEle     = 230.6250;
kIntEle     = 5.625;
ele         = kMinEle : kIntEle : kMaxEle;

azi         = -45 : 5 : 45;
azi         = [-80, -65, -55, azi, 55, 65, 80];

kNumAzi     = length(hrir_l(:, 1, 1));
kNumEle     = length(hrir_l(1, :, 1));
kEleZeroIdx = 9;
kAziZeroIdx = 13;

kFs         = 44100;
kNumSamples = length(hrir_l);
t           = 0 : 1/kFs : (kNumSamples - 1) * 1/kFs;

kFmin       = 100;
kFmax       = kFs/2;
kNFFT       = 1024;
kQ          = Inf;
logScale    = 1;


%% Freq-Domain Data
aziTarget = -30;
aziTargetIdx = find(azi==aziTarget);

% Zero Elevation
[dBZeroL, rFreq] = freq_resp(squeeze(hrir_l(aziTargetIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
[dBZeroR, rFreq] = freq_resp(squeeze(hrir_r(aziTargetIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);

for i = 1:kNumEle
    [dBL(:, i), rFreq] = freq_resp(squeeze(hrir_l(aziTargetIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    [dBR(:, i), rFreq] = freq_resp(squeeze(hrir_r(aziTargetIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    
    dBLNormal(:, i) = dBL(:, i) ./ dBZeroL;
    dBRNormal(:, i) = dBR(:, i) ./ dBZeroR;
end; 

fig1 = figure(1); fig1.Position = [30 100 600 800];
clims = [-15 15];
ticks = [0.125 0.25 0.5 1.0 2.0 4.0 8.0 16.0];

rEle = -45 : 5.625 : 90;
lEle = length(rEle);
 
subplot(2,1,1); imagesc(rEle, rFreq*1E-3, dBL(:, 1:lEle), clims); colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
title('Magnitude HRTF(e)'), xlabel('Src Elevation [\circ]'), ylabel('Frequency [kHz]')

subplot(2,1,2); imagesc(rEle, rFreq*1E-3, dBLNormal(:, 1:lEle), clims); colorbar;
title('Magnitude HRTF(e) / HRTF(0)'), xlabel('Src Elevation [\circ]'), ylabel('Frequency [kHz]')
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');


%% Range Resolve
r0 = find(rFreq < 500);
r1 = find(500 <= rFreq & rFreq < 2000);
r2 = find(2000 <= rFreq & rFreq < 8000);
r3 = find(8000 <= rFreq & rFreq < 16000);

%
fig2 = figure(2); fig2.Position = [630 100 600 900];

subplot(3,1,1);
imagesc(rEle, rFreq(r3(1):r3(end))*1E-3, dBLNormal(r3(1):r3(end), 1:lEle), clims);
colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
title('Magnitude HRTF(e)'), xlabel('Src Elevation [\circ]'), ylabel('Frequency [kHz]')

subplot(3,1,2);
imagesc(rEle, rFreq(r2(1):r2(end))*1E-3, dBLNormal(r2(1):r2(end), 1:lEle), clims);
colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
title('Magnitude HRTF(e)'), xlabel('Src Elevation [\circ]'), ylabel('Frequency [kHz]')

subplot(3,1,3);
imagesc(rEle, rFreq(r1(1):r1(end))*1E-3, dBLNormal(r1(1):r1(end), 1:lEle), clims);
colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
title('Magnitude HRTF(e)'), xlabel('Src Elevation [\circ]'), ylabel('Frequency [kHz]')


%
fig3 = figure(3); fig3.Position = [1230 100 600 900];

subplot(3,1,1);
imagesc(rEle, rFreq(r3(1):r3(end))*1E-3, dBRNormal(r3(1):r3(end), 1:lEle), clims);
colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
title('Magnitude HRTF(e)'), xlabel('Src Elevation [\circ]'), ylabel('Frequency [kHz]')

subplot(3,1,2);
imagesc(rEle, rFreq(r2(1):r2(end))*1E-3, dBRNormal(r2(1):r2(end), 1:lEle), clims);
colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
title('Magnitude HRTF(e)'), xlabel('Src Elevation [\circ]'), ylabel('Frequency [kHz]')

subplot(3,1,3);
imagesc(rEle, rFreq(r1(1):r1(end))*1E-3, dBRNormal(r1(1):r1(end), 1:lEle), clims);
colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
title('Magnitude HRTF(e)'), xlabel('Src Elevation [\circ]'), ylabel('Frequency [kHz]')
