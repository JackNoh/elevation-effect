function [B, A, H]  = peakFilter(fc, gain, Q, fs, N)

K   = tan(pi*fc/fs);
V0  = 10^(gain/20);

if gain >= 0
  denom   = 1+K/Q+K^2;
  B(1)  = ( 1+K*V0/Q+K^2 ) / denom;
  B(2)  = ( 2*(K^2-1) ) / denom;
  B(3)  = ( 1-K*V0/Q+K^2 ) / denom;
  
  A(1)  = 1;
  A(2)  = ( 2*(K^2-1) ) / denom;
  A(3)  = ( 1-K/Q+K^2 ) / denom;
  
else
  denom   = 1+K/(V0*Q)+K^2;
  B(1)  = ( 1+K/Q+K^2 ) / denom;
  B(2)  = ( 2*(K^2-1) ) / denom;
  B(3)  = ( 1-K/Q+K^2 ) / denom;
  
  A(1)  = 1;
  A(2)  = ( 2*(K^2-1) ) / denom;
  A(3)  = ( 1-K/(V0*Q)+K^2 ) / denom;
end
H   = freqz(B, A, N);
end