clear all;
close all;

chId        = {'left30', 'left80', 'left100', 'left150', ...
  'right30', 'right80', 'right100', 'right150', ...
  'center', 'back'};
sampleRate  = {'', '_44k'};
N           = 512;
pinkSpectrum  = (1:N+1)'.^(-1/2);

for ii = 1 : length(chId)
  for jj = 1 : length(sampleRate)
    fName   = sprintf('%s', 'GSA_direct_', chId{ii}, sampleRate{jj}, '.wav');
    [src, fs]   = audioread(fName);
    [~, lidx]   = max(abs(src(:,1)));
    [~, ridx]   = max(abs(src(:,2)));
    idx         = min([lidx ridx]);
    
%     [B, A, H]   = peakFilter(7500, 10, 4, fs, N+1);
%     [B, A, H]   = shelving(fs, 8500, -6, 'HF', N+1);
    [c, lags]   = xcorr( src(:,1), src(:,2), 'coeff');
    s           = lags( abs(c) == max(abs(c)) );
    
    freq        = (0:N)'/(2*N)*fs;
    %     [B, A, H]   = shelving(fs, 8500, -6, 'HF', N+1);
    %     resp        = [abs(H) abs(H)];
    %     ofb         = octaveFilterBank('1/3 octave', fs, 'FrequencyRange', [22 24000]);
    %     filterOut   = ofb(src);
    outSig  = zeros(N,2);
    
    frag  = src;
%     frag  = filter(B, A, src(1:end, :));
    FRAG  = fft(frag, 2*N);
    phase = angle(FRAG);
    mag   = abs(FRAG);
    
    mag   = fftshift(mag);
    min_phase   = unwrap(-imag(hilbert(log(abs(mag)))));
    
    newFRAG   = mag .* exp(1i*min_phase);
    newFRAG   = ifftshift(newFRAG);
    newFrag   = real( ifft( newFRAG ) );
    newFrag   = circshift(newFrag, idx-1);
    
    if s < 0
      newFrag(:,2) = circshift(newFrag(:,2), abs(s));
    elseif s > 0
      newFrag(:,1) = circshift(newFrag(:,1), abs(s));
    end

%     newFrag   = filter(B, A, filter(B, A, newFrag));
    outName   = sprintf('%s', 'GSA_direct_', chId{ii}, sampleRate{jj}, '_minPhase.wav');
    audiowrite(outName, newFrag(1:192, :), fs);

  end
  
  
%   outName   = sprintf('%s', 'IoSR_rsmp_smooth_', chId{ii}, sampleRate{jj}, '.wav');
%   audiowrite(outName, outSig(1:N*2, :), fs);
  
  
  
end


