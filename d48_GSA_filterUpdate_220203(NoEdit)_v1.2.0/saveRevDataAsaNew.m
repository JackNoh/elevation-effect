
clear;

outPaths    = { 'res-IoSR', 'res-IoSR', 'res-newGSA', 'res-newGSA' };

pathnames   = { 'IoSR_EarlyLate_44k', 'IoSR_EarlyLate_48k', 'newGSA_44k_EarlyOnly', 'newGSA_48k_EarlyOnly' };
latenames   = { 'IoSR(byP)_rsmp_late_44k', 'IoSR(byP)_rsmp_late_48k', 'IoSR(byP)_rsmp_late_44k', 'IoSR(byP)_rsmp_late_48k' };
filePrefix  = { 'IoSR_rsmp_', 'IoSR_rsmp_', 'GSA_rsmp_', 'GSA_rsmp_' };
filePostfix = { '_early_44k.wav', '.wav', '_early_44k.wav', '.wav' };

dirPath     = 'averagedHrir4GSA';
dirPrefix   = { 'hrir_', 'hrir_', 'hrir_', 'hrir_' };
dirPostfix  = { '_44k.wav', '_48k.wav', '_44k.wav', '_48k.wav' };

fileNames   = { 'IoSRBrtf', 'IoSRBrtf', 'asaBrtfNew', 'asaBrtfNew' };
numIns      = 4;
gains       = 10^(-3/20);

%%
for ins=1:numIns
  count = 0;
  azis = [-30,30,-80,80,-100,100,-150,150,0,180];
  [r,fs] = audioread([cell2mat(pathnames(ins)) '/' cell2mat(latenames(ins)) '.wav']);
  [B, A, ~] = peakFilter(85, 0, 2, fs, 513);
  [BS, AS, ~] = shelving(fs, 12500, -3, 'HF', 513);
  
  % below is optional
  r = filter(B, A, r);
  for item=1:length(azis)
    if azis(item) == 180  
      dname = [dirPath '/' cell2mat(dirPrefix(ins)) 'back' cell2mat(dirPostfix(ins))];
      fname = [cell2mat(pathnames(ins)) '/' cell2mat(filePrefix(ins)) 'back' cell2mat(filePostfix(ins))];
%       dUpName = [dirPath '/' cell2mat(dirPrefix(ins)) 'up_back' cell2mat(dirPostfix(ins))];
%       dDwName = [dirPath '/' cell2mat(dirPrefix(ins)) 'down_back' cell2mat(dirPostfix(ins))];
    elseif azis(item) < 0
      dname = [dirPath '/' cell2mat(dirPrefix(ins)) 'left' num2str(abs(azis(item))) cell2mat(dirPostfix(ins))];
      fname = [cell2mat(pathnames(ins)) '/' cell2mat(filePrefix(ins)) 'left' num2str(abs(azis(item))) cell2mat(filePostfix(ins))];
%       dUpName = [dirPath '/' cell2mat(dirPrefix(ins)) 'up_left' num2str(abs(azis(item))) cell2mat(dirPostfix(ins))];
%       dDwName = [dirPath '/' cell2mat(dirPrefix(ins)) 'down_left' num2str(abs(azis(item))) cell2mat(dirPostfix(ins))];
    elseif azis(item) > 0
      dname = [dirPath '/' cell2mat(dirPrefix(ins)) 'right' num2str(abs(azis(item))) cell2mat(dirPostfix(ins))];
      fname = [cell2mat(pathnames(ins)) '/' cell2mat(filePrefix(ins)) 'right' num2str(abs(azis(item))) cell2mat(filePostfix(ins))];
%       dUpName = [dirPath '/' cell2mat(dirPrefix(ins)) 'up_right' num2str(abs(azis(item))) cell2mat(dirPostfix(ins))];
%       dDwName = [dirPath '/' cell2mat(dirPrefix(ins)) 'down_right' num2str(abs(azis(item))) cell2mat(dirPostfix(ins))];
    else
      dname = [dirPath '/' cell2mat(dirPrefix(ins)) 'center' cell2mat(dirPostfix(ins))];
      fname = [cell2mat(pathnames(ins)) '/' cell2mat(filePrefix(ins)) 'center' cell2mat(filePostfix(ins))];
%       dUpName = [dirPath '/' cell2mat(dirPrefix(ins)) 'up_center' cell2mat(dirPostfix(ins))];
%       dDwName = [dirPath '/' cell2mat(dirPrefix(ins)) 'down_center' cell2mat(dirPostfix(ins))];
    end

    disp([dname '    ' fname]);

    d  = audioread ( dname );
%     dUp  = audioread ( dUpName );
%     dDw  = audioread ( dDwName );

    e  = audioread ( fname );
    e  = filter(B, A, e);
    [c1, lags1]   = xcorr(d(:,1), d(:,2), 'coeff');
    s1            = lags1( abs(c1) == max(abs(c1)));
    
    [c2, lags2]   = xcorr(e(:,1), e(:,2), abs(s1), 'coeff');
    s2            = lags2( abs(c2) == max(abs(c2)) );
    if s1 <= 0
      e(:,2)      = [zeros(s2-s1, 1); e(1:end-(s2-s1), 2)];
      addDelay    = round( 6*sin(abs(azis(item))*pi/180) );
      d(:,2)      = [zeros(addDelay, 1); d(1:end-addDelay, 2)];
    elseif s1 > 0
      e(:,1)      = [zeros(s1-s2, 1); e(1:end-(s1-s2), 1)];
      addDelay    = round( 6*sin(abs(azis(item))*pi/180) );
      d(:,1)      = [zeros(addDelay,1); d(1:end-addDelay, 1)];
    end

    d   = d * 10^(0.8/20);
    e   = e * gains * 10^(-0.3/20);

    %%
    for ch=1:2
      fp    = fopen ( sprintf('%s/%s_%02d_%02d_v6.dat', cell2mat(outPaths(ins)), cell2mat(fileNames(ins)), round(fs/1000), count), 'wb' );
%       fpUp  = fopen ( sprintf('%s/%s_%02d_%02d_v6.dat', cell2mat(outPaths(ins)), cell2mat(fileNames(ins)), round(fs/1000), count + 20), 'wb' );
%       fpDw  = fopen ( sprintf('%s/%s_%02d_%02d_v6.dat', cell2mat(outPaths(ins)), cell2mat(fileNames(ins)), round(fs/1000), count + 40), 'wb' );

      % direct part
      input   = [d(:,ch); zeros(1024-length(d),1)];
      resp    = fft(input);
      fwrite ( fp, real(resp(1)), 'float32' );
      fwrite ( fp, real(resp(513)), 'float32' );
      for n=2:512
        fwrite ( fp, real(resp(n)), 'float32' );
        fwrite ( fp, imag(resp(n)), 'float32' );
      end

%       input   = [dUp(:,ch); zeros(1024-length(dUp),1)];
%       resp    = fft(input);
%       fwrite ( fpUp, real(resp(1)), 'float32' );
%       fwrite ( fpUp, real(resp(513)), 'float32' );
%       for n=2:512
%         fwrite ( fpUp, real(resp(n)), 'float32' );
%         fwrite ( fpUp, imag(resp(n)), 'float32' );
%       end
%
%       input   = [dDw(:,ch); zeros(1024-length(dDw),1)];
%       resp    = fft(input);
%       fwrite ( fpDw, real(resp(1)), 'float32' );
%       fwrite ( fpDw, real(resp(513)), 'float32' );
%       for n=2:512
%         fwrite ( fpDw, real(resp(n)), 'float32' );
%         fwrite ( fpDw, imag(resp(n)), 'float32' );
%       end

      % early part
      resp  = zeros ( 1024,2 );
      for frame=1:2
        from  = (frame-1)*512+1;
        to    = frame*512;

        input = [e(from:to,ch); zeros(512,1)];
        resp(1:1024,frame)   = fft(input);

        fwrite ( fp, real(resp(1,frame)), 'float32' );
        fwrite ( fp, real(resp(513,frame)), 'float32' );
        for n=2:512
          fwrite ( fp, real(resp(n,frame)), 'float32' );
          fwrite ( fp, imag(resp(n,frame)), 'float32' );
        end      
      end

      % rev part
      if item==9 % if center
        resp  = zeros ( 1024,14 );
        for frame=1:14
          from  = (frame-1)*512+1;
          to    = frame*512;

          input = [r(from:to,ch); zeros(512,1)];
          resp(1:1024,frame)   = fft(input);

          fwrite ( fp, real(resp(1,frame)), 'float32' );
          fwrite ( fp, real(resp(513,frame)), 'float32' );
          for n=2:512
            fwrite ( fp, real(resp(n,frame)), 'float32' );
            fwrite ( fp, imag(resp(n,frame)), 'float32' );
          end      
        end
      end

      fclose(fp);
%       fclose(fpUp);
%       fclose(fpDw);
      count = count + 1;
    end
  end  
end

system ( 'cp ./res-IoSR/* /Users/James/Dev/BTA/gsa-research-hq/native_library_new/src/resources/brtf');
system ( 'cp ./res-newGSA/* /Users/James/Dev/BTA/gsa-research-hq/native_library_new/src/resources/brtf');

