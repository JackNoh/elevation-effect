clc;
clear;
close all;

dirPath         = 'averagedHrir4GSA';
dirFilePrefix   = 'hrir_';
dirFilePostfix  = { '_44k.wav', '_48k.wav', '_44k.wav', '_48k.wav' };

revPath             = { 'IoSR_EarlyLate_44k', 'IoSR_EarlyLate_48k', 'newGSA_44k_EarlyOnly', 'newGSA_48k_EarlyOnly' };
lateFile            = { 'IoSR(byP)_rsmp_late_44k', 'IoSR(byP)_rsmp_late_48k', 'IoSR(byP)_rsmp_late_44k', 'IoSR(byP)_rsmp_late_48k' };
earlyFilePrefix     = { 'IoSR_rsmp_', 'IoSR_rsmp_', 'GSA_rsmp_', 'GSA_rsmp_' };
earlyFilePostfix    = { '_early_44k.wav', '.wav', '_early_44k.wav', '.wav' };

outPath         = { 'res-IoSR', 'res-IoSR', 'res-newGSA', 'res-newGSA' };
outFileName     = { 'IoSRBrtf', 'IoSRBrtf', 'asaBrtfNew', 'asaBrtfNew' };

elePostfix = { ''                                                               ...
               'up1_'  'up2_'  'up3_'  'up4_'  'up5_'  'up6_'  'up7_'  'up8_'	...
               'up9_'  'up10_' 'up11_' 'up12_' 'up13_' 'up14_' 'up15_'          ...
               'dw8_'  'dw7_'  'dw6_'  'dw5_'  'dw4_'  'dw3_'  'dw2_'  'dw1_'   ...
             };


%% Configuration
numIns = 4;
azis = [-30,30,-80,80,-100,100,-150,150,0,180];
gains = 10^(-3/20);


%% Generate .dat File
for ins=1:numIns

    count = 0;
    [r,fs] = audioread([revPath{ins} '/' lateFile{ins} '.wav']);
    [B, A, ~] = peakFilter(85, 0, 2, fs, 513);
    [BS, AS, ~] = shelving(fs, 12500, -3, 'HF', 513);
    r = filter(B, A, r);
    
    for ee=1:length(elePostfix)

        for aa=1:length(azis)

            if azis(aa) == 180
                dname = [dirPath '/' dirFilePrefix elePostfix{ee} 'back' dirFilePostfix{ins}];
                fname = [revPath{ins} '/' earlyFilePrefix{ins} 'back' earlyFilePostfix{ins}];
            elseif azis(aa) < 0
                dname = [dirPath '/' dirFilePrefix elePostfix{ee} 'left' num2str(abs(azis(aa))) dirFilePostfix{ins}];
                fname = [revPath{ins} '/' earlyFilePrefix{ins} 'left' num2str(abs(azis(aa))) earlyFilePostfix{ins}];
            elseif azis(aa) > 0
                dname = [dirPath '/' dirFilePrefix elePostfix{ee} 'right' num2str(abs(azis(aa))) dirFilePostfix{ins}];
                fname = [revPath{ins} '/' earlyFilePrefix{ins} 'right' num2str(abs(azis(aa))) earlyFilePostfix{ins}];
            else
                dname = [dirPath '/' dirFilePrefix elePostfix{ee} 'center' dirFilePostfix{ins}];
                fname = [revPath{ins} '/' earlyFilePrefix{ins} 'center' earlyFilePostfix{ins}];
            end

            disp([dname '    ' fname]);
            d           = audioread ( dname );
            [c1, lags1]	= xcorr(d(:,1), d(:,2), 'coeff');
            s1          = lags1( abs(c1) == max(abs(c1)));
            if (ee == 1)
                e           = audioread ( fname );
                e           = filter(B, A, e);
                [c2, lags2] = xcorr(e(:,1), e(:,2), abs(s1), 'coeff');
                s2          = lags2( abs(c2) == max(abs(c2)) );
            end

            if s1 <= 0
                if (ee == 1)
                    e(:,2) = [zeros(s2-s1, 1); e(1:end-(s2-s1), 2)];
                end
                addDelay = round( 6*sin(abs(azis(aa))*pi/180) );
                d(:,2) = [zeros(addDelay, 1); d(1:end-addDelay, 2)];
            elseif s1 > 0
                if (ee == 1)
                    e(:,1) = [zeros(s1-s2, 1); e(1:end-(s1-s2), 1)];
                end
                addDelay = round( 6*sin(abs(azis(aa))*pi/180) );
                d(:,1) = [zeros(addDelay,1); d(1:end-addDelay, 1)];
            end

            d   = d * 10^(0.8/20);
            e   = e * gains * 10^(-0.3/20);

            % For-loop Ipsi-Contra
            for ch=1:2
                fp = fopen ( sprintf('%s/%s_%02d_%02d_v6.dat', outPath{ins}, outFileName{ins}, round(fs/1000), count), 'wb' );

                % direct part
                input   = [d(:,ch); zeros(1024-length(d),1)];
                resp    = fft(input);
                fwrite ( fp, real(resp(1)), 'float32' );
                fwrite ( fp, real(resp(513)), 'float32' );
                for n=2:512
                    fwrite ( fp, real(resp(n)), 'float32' );
                    fwrite ( fp, imag(resp(n)), 'float32' );
                end

                % reverb part (when elevation = 0)
                if (ee == 1)

                    % early
                    resp  = zeros ( 1024,2 );
                    for frame=1:2
                        from  = (frame-1)*512+1;
                        to    = frame*512;
                        input = [e(from:to,ch); zeros(512,1)];
                        resp(1:1024,frame)   = fft(input);
                        fwrite ( fp, real(resp(1,frame)), 'float32' );
                        fwrite ( fp, real(resp(513,frame)), 'float32' );
                        for n=2:512
                            fwrite ( fp, real(resp(n,frame)), 'float32' );
                            fwrite ( fp, imag(resp(n,frame)), 'float32' );
                        end
                    end

                    % late
                    if aa==9 % if center
                        resp  = zeros ( 1024,14 );
                        for frame=1:14
                            from  = (frame-1)*512+1;
                            to    = frame*512;
                            input = [r(from:to,ch); zeros(512,1)];
                            resp(1:1024,frame)   = fft(input);
                            fwrite ( fp, real(resp(1,frame)), 'float32' );
                            fwrite ( fp, real(resp(513,frame)), 'float32' );
                            for n=2:512
                                fwrite ( fp, real(resp(n,frame)), 'float32' );
                                fwrite ( fp, imag(resp(n,frame)), 'float32' );
                            end
                        end
                    end

                end

                fclose(fp);
                count = count + 1;

            end % for ch=1:2

        end % for item=1:length(azis)

    end % ee=1:length(elePostfix)

end % ins=1:numIns

% system ( 'cp ./res-IoSR/* /Users/James/Dev/BTA/gsa-research-hq/native_library_new/src/resources/brtf');
% system ( 'cp ./res-newGSA/* /Users/James/Dev/BTA/gsa-research-hq/native_library_new/src/resources/brtf');

