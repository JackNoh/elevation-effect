function [B, A, H]   = shelving(fs, fc, G, F, N)
% fs : sampling rate
% fc : cutoff frequency
% G : gain in dB scale
% F : LF or HF in character
% N : the number of frequency bin 

K   = tan(pi*fc/fs);
V0  = 10^(G/20);

if strcmp(F, 'LF')
  if G >= 0 % boost
    denom   = 1+sqrt(2)*K+K^2;
    B       = [1+sqrt(2*V0)*K+V0*K^2 2*(V0*K^2-1) 1-sqrt(2*V0)*K+V0*K^2] / denom;
    A       = [denom 2*(K^2-1) 1-sqrt(2)*K+K^2] / denom;
  else % cut
    denom   = V0+sqrt(2*V0)*K+K^2;
    B       = [V0*(1+sqrt(2)*K+K^2) 2*V0*(K^2-1) V0*(1-sqrt(2)*K+K^2)] / denom;
    A       = [denom 2*(K^2-V0) V0-sqrt(2*V0)*K+K^2]/denom;
  end
elseif strcmp(F, 'HF')
  if G >= 0
    denom   = 1+sqrt(2)*K+K^2;
    B       = [V0+sqrt(2*V0)*K+K^2 2*(K^2-V0) V0-sqrt(2*V0)*K+K^2] / denom;
    A       = [denom 2*(K^2-1) 1-sqrt(2)*K+K^2] / denom;
  else
    denom   = 1+sqrt(2*V0)*K+V0*K^2;
    B       = [V0*(1+sqrt(2)*K+K^2) 2*V0*(K^2-1) V0*(1-sqrt(2)*K+K^2)] / denom;
    A       = [denom 2*(V0*K^2-1) 1-sqrt(2*V0)*K+V0*K^2] / denom;
  end
end

H           = freqz(B, A, N);


