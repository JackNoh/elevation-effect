clear all;
close all;

degree  = [30 80 100 150];
fs      = 48000;
for ii = 1 : length(degree)
  leftName  = sprintf('%s', 'hrir_left', num2str(degree(ii)), '_', num2str(round(fs/1000)), 'k.wav');
  rightName = sprintf('%s', 'hrir_right', num2str(degree(ii)), '_', num2str(round(fs/1000)), 'k.wav');
  lCh       = audioread(leftName);
  rCh       = audioread(rightName);
  
  ipsiL   = sqrt( sum( lCh(:,1).^2 ) );
  ipsiR   = sqrt( sum( rCh(:,2).^2 ) );
  if ipsiL >= ipsiR
    lCh(:,1)  = lCh(:,1) * ipsiR/ipsiL;
  else
    rCh(:,2)  = rCh(:,2) * ipsiL/ipsiR;
  end
  
  contraL   = sqrt( sum( lCh(:,2).^2 ) );
  contraR   = sqrt( sum( rCh(:,1).^2 ) );
  
  if contraL >= contraR
    lCh(:,2) = lCh(:,2) * contraR/contraL;
  else
    rCh(:,1) = rCh(:,1) * contraL/contraR;
  end
  
  audiowrite(leftName, lCh, fs);
  audiowrite(rightName, rCh, fs);
  
  
  
end

% center  = audioread('hrir_center_48k.wav');
% plot(center);
