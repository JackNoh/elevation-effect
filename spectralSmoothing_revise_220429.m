clc;
clear;
close all;

fs = 48000;

%% Configuration - GSA WS
azi = [1 7 13 19 25]; % -80, -30, 0, 30, 80
aziFrontPostfix = {'left80', 'left30', 'center', 'right30', 'right80'};
aziRearPostFix = {'left100', 'left150', 'back', 'right150', 'right100'};

ele = 1:49; % -45 ~ 225
eleAngle = -50.625 + 5.625 * ele;
elePostfix = { '_dw8'  '_dw7'  '_dw6'  '_dw5'  '_dw4'  '_dw3'  '_dw2'  '_dw1'  ''  ...
               '_up1'  '_up2'  '_up3'  '_up4'  '_up5'  '_up6'  '_up7'  '_up8'      ...
               '_up9'  '_up10' '_up11' '_up12' '_up13' '_up14' '_up15' '_up16'     ...
               '_up15' '_up14' '_up13' '_up12' '_up11' '_up10' '_up9'  '_up8'      ...
               '_up7'  '_up6'  '_up5'  '_up4'  '_up3'  '_up2'  '_up1'  ''          ...
               '_dw1'  '_dw2'  '_dw3'  '_dw4'  '_dw5'  '_dw6'  '_dw7'  '_dw8'      };

folder  = './CIPIC_hrtf_database/standard_hrir_database/';
outFolder   = './averagedHrir4GSA2/';
if ~exist(outFolder, 'dir')
  mkdir(outFolder);
end

D           = dir(folder);
N           = 512;
freq        = (0:N)'/(2*N)*fs;

refFreq         = 1000;
[~, freqIdx]    = min(abs(freq-refFreq));

% Common Tone Filter
[B2, A2, H2]  = peakFilter( 170, 0, 6, fs, N+1);
[B1, A1, ~]   = shelving(fs, 300, 0, 'LF', N+1);

nn          = 0;
itd         = zeros(1, length(ele));
exponent    = 1/2;


%% Compensate ASA Response
[refl, fs_L]    = audioread('AirpodPro_-5stepVol_NC_On_SA_Off_brir_FL.wav');
[~, idx]        = max(abs(refl(:,1)));
refl            = refl(idx-63:idx+8192-64, :);
[refr, fs_R]    = audioread('AirpodPro_-5stepVol_NC_On_SA_Off_brir_FR.wav');
[~, idx]        = max(abs(refr(:, 2)));
refr            = refr(idx-63:idx+8192-64, :);

newRef = (refl(:,1) + refr(:,2))/2;
if fs_L ~= fs
  newRef  = resample(newRef, fs, fs_L);
end
newRef = newRef(1:N, :);

REFL            = fft([newRef; zeros(N,1)]);
[~, tempIdx]    = min( abs( freq-refFreq ) );

if freq(tempIdx,1) > refFreq
  val1      = abs(REFL( tempIdx-1, 1));
  val2      = abs(REFL( tempIdx, 1));
  refVal    = ( (freq(tempIdx, 1) - refFreq) * val1 + (refFreq - freq(tempIdx-1, 1)) * val2 ) / ... 
              ( freq(tempIdx, 1) - freq(tempIdx-1, 1) );
elseif freq(tempIdx, 1) < refFreq
  val1      = abs(REFL( tempIdx, 1));
  val2      = abs(REFL( tempIdx+1, 1));
  refVal    = ( (freq(tempIdx+1, 1) - refFreq) * val1 + (refFreq - freq(tempIdx, 1)) * val2 ) / ...
              ( freq(tempIdx+1, 1) - freq(tempIdx, 1) );
else
  refVal    = abs( REFL(tempIdx, 1));
end

REFL                    = abs(REFL) ./ refVal;
[upperLim]              = find(REFL(1:N+1, 1)>1, 1, 'last');
REFL(upperLim+1:N+1, 1) = 1;

INVRESP                         = 1./REFL(1:N+1, 1);
INVRESP(1:round(tempIdx/2), 1)  = 1;


%% Create HRTF
for aa = 1 : length(azi)
    
    for ee = 1 : length(ele)
        nn  = 0;
        itd = 0;
        sHl = zeros(N*2, 1);
        sHr = zeros(N*2, 1);

        for dd = 1 : length(D)
            if D(dd).isdir == 1 & D(dd).name(1) ~= '.' & D(dd).name(1:2) == 'su'
                nn = nn + 1;
                fileName = sprintf('%s', folder, D(dd).name, '/hrir_final.mat');
                load(fileName);

                hl = squeeze(hrir_l(azi(aa), ele(ee), :));
                hr = squeeze(hrir_r(azi(aa), ele(ee), :));
                if fs == 48000
                    hl = resample(hl, 48000, 44100);
                    hr = resample(hr, 48000, 44100);
                end

                [c, lags] = xcorr(hl, hr, 'coeff');
                s = lags( abs(c) == max(abs(c)) );
                itd = itd + s;

                Hl = fft(hl, N*2);
                Hr = fft(hr, N*2);

                sHl = sHl + abs(Hl);
                sHr = sHr + abs(Hr);
            end
        end
        sHl = sHl / nn;
        sHr = sHr / nn;
        itd = round(itd / nn);

        if aa <= 3
            refVal = sHl(freqIdx);
            ild  = sHr(1:N+1) ./ sHl(1:N+1);
        else
            refVal = sHr(freqIdx);
            ild  = sHl(1:N+1) ./ sHr(1:N+1);
        end
        sHl = sHl / refVal;
        sHr = sHr / refVal;

        HRTF = zeros(N+1, 2);
        if aa <= 3
            idx = find( sHl(1:N, 1) > 1 );
            HRTF(:, 1) = sHl(1:N+1, 1);
            HRTF(idx, 1) = sHl(idx, 1) .^ exponent;
            HRTF(1:N+1, 1) = HRTF(1:N+1, 1) .* INVRESP;
            HRTF(1:N+1, 2) = HRTF(1:N+1, 1) .* ild;
        else
            idx = find( sHr(1:N, 1) > 1 );
            HRTF(:, 2) = sHr(1:N+1, 1);
            HRTF(idx, 2) = sHr(idx, 1) .^ exponent;
            HRTF(1:N+1, 2) = HRTF(1:N+1, 2) .* INVRESP;
            HRTF(1:N+1, 1) = HRTF(1:N+1, 2) .* ild;
        end
        HRTF(N+2:2*N, :) = HRTF(N:-1:2, :);

        HRTF        = fftshift(HRTF);
        minPhaseF   = unwrap(-imag(hilbert(log(abs(HRTF)))));
        HRTF        = HRTF .* exp(1i*minPhaseF);
        HRTF        = ifftshift(HRTF);

        hrir = real( ifft( HRTF ) );

        % Tone Tuning
        if aa == 3 % Center
            [B3, A3, ~] = peakFilter( 9000, -1.5, 4, fs, N+1);
            hrir   = filter(B3, A3, filter(B1, A1, filter(B2, A2, hrir)));
        elseif aa == 2 % -30
            [B3, A3, ~] = peakFilter( 9000, -2.5, 4, fs, N+1);
            hrir = filter(B1, A1, filter(B2, A2, hrir));
            hrir(:,1) = filter(B3, A3, hrir(:,1));
        elseif aa == 1 % -80
            [B3, A3, ~] = peakFilter( 9000, -4, 4, fs, N+1);
            hrir = filter(B1, A1, filter(B2, A2, hrir));
            hrir(:,1) = filter(B3, A3, hrir(:,1));
        elseif aa == 4
            [B3, A3, ~] = peakFilter( 9000, -2.5, 4, fs, N+1);
            hrir = filter(B1, A1, filter(B2, A2, hrir));
            hrir(:,2) = filter(B3, A3, hrir(:,2));
        elseif aa == 5
            [B3, A3, ~] = peakFilter( 9000, -4, 4, fs, N+1);
            hrir = filter(B1, A1, filter(B2, A2, hrir));
            hrir(:,2) = filter(B3, A3, hrir(:,2));
        end

        if fs == 48000
            default = 16;
        else
            default = 15;
        end

        if itd < 0
            hrir(:,1)  = circshift(hrir(:,1), default);
            hrir(:,2)  = circshift(hrir(:,2), default + abs(itd));
        elseif itd > 0
            hrir(:,1)  = circshift(hrir(:,1), default + abs(itd));
            hrir(:,2)  = circshift(hrir(:,2), default);
        elseif itd == 0
            hrir       = circshift(hrir, default);
        end

        if (ee < 25) % -45 ~ 84.325
            writeName = sprintf('%s', outFolder, 'hrir', elePostfix{ee}, '_', aziFrontPostfix{aa}, '_', num2str(round(fs/1000)), 'k.wav');
        elseif (25 < ee) % 95.675 ~ 225
            writeName = sprintf('%s', outFolder, 'hrir', elePostfix{ee}, '_', aziRearPostFix{aa}, '_', num2str(round(fs/1000)), 'k.wav');
        end

        if (ee ~= 25)
            audiowrite(writeName, hrir(1:192, :) * 0.7, fs);
        end

    end % ee = 1 : length(ele)

end % aa = 1 : length(azi)

