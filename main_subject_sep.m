clc;
clear;
close all;

dataArray = {'003', '008', '009', '010', '011', ...
             '012', '015', '017', '018', '019', ...
             '020', '021', '027', '028', '033', ...
             '040', '044', '048', '050', '051', ...
             '058', '059', '060', '061', '065', ...
             '119', '124', '126', '127', '131', ...
             '133', '134', '135', '137', '147', ...
             '148', '152', '153', '154', '155', ...
             '156', '158', '162', '163', '165'};

% addpath 'CIPIC_hrtf_database/standard_hrir_database';
hrir_l_avg = zeros(25, 50, 200);
hrir_r_avg = zeros(25, 50, 200);

for i = 1:length(dataArray)
    load(['CIPIC_hrtf_database/standard_hrir_database/subject_', dataArray{i}, '/hrir_final.mat']);
    hrir_l_avg = hrir_l_avg + hrir_l;
    hrir_r_avg = hrir_r_avg + hrir_r;
end;

hrir_l_avg = hrir_l_avg / length(dataArray);
hrir_r_avg = hrir_r_avg / length(dataArray);


%% Params
kMinEle     = -45;
kMaxEle     = 230.6250;
kIntEle     = 5.625;
ele         = kMinEle : kIntEle : kMaxEle;

azi         = -45 : 5 : 45;
azi         = [-80, -65, -55, azi, 55, 65, 80];

kNumAzi     = length(hrir_l_avg(:, 1, 1));
kNumEle     = length(hrir_l_avg(1, :, 1));

kEleZeroIdx = find(ele==0);
kAziZeroIdx = find(azi==0);

kFs         = 44100;
kNumSamples = length(hrir_l_avg);
t           = 0 : 1/kFs : (kNumSamples - 1) * 1/kFs;

% FFT Params
kFmin       = 100;
kFmax       = 12800;
kNFFT       = 1024;
kQ          = 64;
logScale    = 1;

% Plot Params
clims1 = [-30 20];
clims2 = [-20 20];
ticks = [100 200 400 800 1600 3200 6400 12800];


%% Front | UP
aziIdx = find(azi==0);

idx1 = 4; color1 = [0.5, 0, 0.5];
idx2 = 8; color2 = [1, 0, 1];

[dBLZero, f] = freq_resp(squeeze(hrir_l_avg(aziIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
[dBRZero, f] = freq_resp(squeeze(hrir_r_avg(aziIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
for i = 1:kNumEle
    [dBL(:, i), f] = freq_resp(squeeze(hrir_l_avg(aziIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    [dBR(:, i), f] = freq_resp(squeeze(hrir_r_avg(aziIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    dBLNormal(:, i) = dBL(:, i) - dBLZero;
    dBRNormal(:, i) = dBR(:, i) - dBRZero;
end; 

fig1 = figure(1); hold on;
fig1.Position = [100 100 1600 800];

subplot(2,2,1);
plot(f, dBL(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims1)
plot(f, dBL(:, kEleZeroIdx + idx1), 'color', color1);
plot(f, dBL(:, kEleZeroIdx + idx2), 'color', color2);
title('dBL(0) | UP'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,2);
plot(f, dBR(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims1)
plot(f, dBR(:, kEleZeroIdx + idx1), 'color', color1);
plot(f, dBR(:, kEleZeroIdx + idx2), 'color', color2);
title('dBR(0) | UP'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,3);
plot(f, dBLNormal(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims2)
plot(f, dBLNormal(:, kEleZeroIdx + idx1), 'color', color1);
plot(f, dBLNormal(:, kEleZeroIdx + idx2), 'color', color2);
title('Normalized dBL(0) | UP'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,4);
plot(f, dBRNormal(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim([-60 60])
plot(f, dBRNormal(:, kEleZeroIdx + idx1), 'color', color1);
plot(f, dBRNormal(:, kEleZeroIdx + idx2), 'color', color2);
title('Normalized dBR(0) | UP'), set(gca, 'XTick', ticks, 'Xscale', 'log');


%% -30 L | UP
aziIdx = find(azi==-30);

idx1 = 4; color1 = [0.5, 0, 0.5];
idx2 = 8; color2 = [1, 0, 1];

% Zero Elevation
[dBLZero, f] = freq_resp(squeeze(hrir_l_avg(aziIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
[dBRZero, f] = freq_resp(squeeze(hrir_r_avg(aziIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
for i = 1:kNumEle
    [dBL(:, i), f] = freq_resp(squeeze(hrir_l_avg(aziIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    [dBR(:, i), f] = freq_resp(squeeze(hrir_r_avg(aziIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    dBLNormal(:, i) = dBL(:, i) - dBLZero;
    dBRNormal(:, i) = dBR(:, i) - dBRZero;
end;

fig2 = figure(2); hold on;
fig2.Position = [100 100 1600 800];

subplot(2,2,1);
plot(f, dBL(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims1)
plot(f, dBL(:, kEleZeroIdx + idx1), 'color', color1);
plot(f, dBL(:, kEleZeroIdx + idx2), 'color', color2);
title('dBL(-30) | UP'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,2);
plot(f, dBR(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims1)
plot(f, dBR(:, kEleZeroIdx + idx1), 'color', color1);
plot(f, dBR(:, kEleZeroIdx + idx2), 'color', color2);
title('dBR(-30) | UP'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,3);
plot(f, dBLNormal(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims2)
plot(f, dBLNormal(:, kEleZeroIdx + idx1), 'color', color1);
plot(f, dBLNormal(:, kEleZeroIdx + idx2), 'color', color2);
title('Normalized dBL(-30) | UP'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,4);
plot(f, dBRNormal(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims2)
plot(f, dBRNormal(:, kEleZeroIdx + idx1), 'color', color1);
plot(f, dBRNormal(:, kEleZeroIdx + idx2), 'color', color2);
title('Normalized dBR(-30) | UP'), set(gca, 'XTick', ticks, 'Xscale', 'log');


%% Front | DOWN
aziIdx = find(azi==0);

idx1 = 4; color1 = [0, 0.3, 0.5];
idx2 = 8; color2 = [0, 0, 1.0];

% Zero Elevation
[dBLZero, f] = freq_resp(squeeze(hrir_l_avg(aziIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
[dBRZero, f] = freq_resp(squeeze(hrir_r_avg(aziIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
for i = 1:kNumEle
    [dBL(:, i), f] = freq_resp(squeeze(hrir_l_avg(aziIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    [dBR(:, i), f] = freq_resp(squeeze(hrir_r_avg(aziIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    dBLNormal(:, i) = dBL(:, i) - dBLZero;
    dBRNormal(:, i) = dBR(:, i) - dBRZero;
end;

fig3 = figure(3); hold on;
fig3.Position = [100 100 1600 800];

subplot(2,2,1);
plot(f, dBL(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims1)
plot(f, dBL(:, kEleZeroIdx - idx1), 'color', color1);
plot(f, dBL(:, kEleZeroIdx - idx2), 'color', color2);
title('dBL(0) | DOWN'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,2);
plot(f, dBR(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims1)
plot(f, dBR(:, kEleZeroIdx - idx1), 'color', color1);
plot(f, dBR(:, kEleZeroIdx - idx2), 'color', color2);
title('dBR(0) | DOWN'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,3);
plot(f, dBLNormal(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims2)
plot(f, dBLNormal(:, kEleZeroIdx - idx1), 'color', color1);
plot(f, dBLNormal(:, kEleZeroIdx - idx2), 'color', color2);
title('Normalized dBL(0) | DOWN'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,4);
plot(f, dBRNormal(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims2)
plot(f, dBRNormal(:, kEleZeroIdx - idx1), 'color', color1);
plot(f, dBRNormal(:, kEleZeroIdx - idx2), 'color', color2);
title('Normalized dBR(0) | DOWN'), set(gca, 'XTick', ticks, 'Xscale', 'log');


%% -30 L | DOWN
aziIdx = find(azi==-30);

idx1 = 4; color1 = [0, 0.3, 0.5];
idx2 = 8; color2 = [0, 0, 1.0];

% Zero Elevation
[dBLZero, f] = freq_resp(squeeze(hrir_l_avg(aziIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
[dBRZero, f] = freq_resp(squeeze(hrir_r_avg(aziIdx, kEleZeroIdx, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
for i = 1:kNumEle
    [dBL(:, i), f] = freq_resp(squeeze(hrir_l_avg(aziIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    [dBR(:, i), f] = freq_resp(squeeze(hrir_r_avg(aziIdx, i, :)), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    dBLNormal(:, i) = dBL(:, i) - dBLZero;
    dBRNormal(:, i) = dBR(:, i) - dBRZero;
end;

fig4 = figure(4); hold on;
fig4.Position = [100 100 1600 800];

subplot(2,2,1);
plot(f, dBL(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims1)
plot(f, dBL(:, kEleZeroIdx - idx1), 'color', color1);
plot(f, dBL(:, kEleZeroIdx - idx2), 'color', color2);
title('dBL(-30) | DOWN'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,2);
plot(f, dBR(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims1)
plot(f, dBR(:, kEleZeroIdx - idx1), 'color', color1);
plot(f, dBR(:, kEleZeroIdx - idx2), 'color', color2);
title('dBR(-30) | DOWN'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,3);
plot(f, dBLNormal(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims2)
plot(f, dBLNormal(:, kEleZeroIdx - idx1), 'color', color1);
plot(f, dBLNormal(:, kEleZeroIdx - idx2), 'color', color2);
title('Normalized dBL(-30) | DOWN'), set(gca, 'XTick', ticks, 'Xscale', 'log');

subplot(2,2,4);
plot(f, dBRNormal(:, kEleZeroIdx), 'k--'); grid on; hold on;
xlim([kFmin kFmax]), ylim(clims2)
plot(f, dBRNormal(:, kEleZeroIdx - idx1), 'color', color1);
plot(f, dBRNormal(:, kEleZeroIdx - idx2), 'color', color2);
title('Normalized dBR(-30) | DOWN'), set(gca, 'XTick', ticks, 'Xscale', 'log');

