clc;
clear;
close all;


%% Data Load
data_path = 'CIPIC_hrtf_database/standard_hrir_database/';
data = dir(data_path);

azi = [1 7 13 19 25]; % -80, -30, 0, 30, 80
ele = [1 9 17 33 41 49]; % -45, 0, 45, 135, 180, 225

out_path = './averageOmniHrtf/';
if ~exist(out_path, 'dir')
    mkdir(out_path);
end

%% Configuration
n       = 512;
fs      = 44100;
freq	= (0:n)' / (2 * n) * fs;

ref             = 1000;
[~, ref_idx]    = min(abs(freq - ref));

output_name = { 'L080_D045', 'L080_F000', 'L080_U045', 'L080_U135', 'L080_R180', 'L080_D225', ...
                'L030_D045', 'L030_F000', 'L030_U045', 'L030_U135', 'L030_R180', 'L030_D225', ...
                'C000_D045', 'C000_F000', 'C000_U045', 'C000_U135', 'C000_R180', 'C000_D225', ...
                'R030_D045', 'R030_F000', 'R030_U045', 'R030_U135', 'R030_R180', 'R030_D225', ...
                'R080_D045', 'R080_F000', 'R080_U045', 'R080_U135', 'R080_R180', 'R080_D225' };

% Un-defined function
% [B2, A2, H2]  = peakFilter( 170, -4, 6, fs, n+1);
% [B1, A1, ~]   = shelving(fs, 300, 0, 'LF', n+1);

% Un-necessary
% destValue = 0.5;
% freqGain  = (1:(destValue-1)/n:destValue)';




%% Data Average

%% Create HRTF

%% Compare w/ Original Data