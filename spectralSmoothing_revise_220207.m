clear all;
close all;

azim  = [13 7 1 19 25]'; % azimuth index for 0, -30, -80, 30, 80 degree
elev  = [9 41]; % elevation index for 0 and 180 elevation angle.

folder      = './CIPIC_hrtf_database/standard_hrir_database/';
outFolder   = './averagedHrir4GSA2/';
if ~exist(outFolder, 'dir')
  mkdir(outFolder);
end

D           = dir(folder);
N           = 512;
fs          = 48000;
freq        = (0:N)'/(2*N)*fs;

refFreq         = 1000;
[~, freqIdx]    = min(abs(freq-refFreq));

fName = {'center', 'left30', 'left80', 'right30', 'right80'};
rName = {'back', 'left150', 'left100', 'right150', 'right100'};


[B2, A2, H2]  = peakFilter( 170, 0, 6, fs, N+1);
[B1, A1, ~]   = shelving(fs, 300, 0, 'LF', N+1);

destValue = 0.5;
freqGain  = (1:(destValue-1)/N:destValue)';
nn        = 0;
itd1      = 0;
itd2      = 0;

[refl, fs_L] = audioread('AirpodPro_-5stepVol_NC_On_SA_Off_brir_FL.wav');
[~, idx]    = max(abs(refl(:,1)));
refl        = refl(idx-63:idx+8192-64, :);

[refr, fs_R] = audioread('AirpodPro_-5stepVol_NC_On_SA_Off_brir_FR.wav');
[~, idx]    = max(abs(refr(:, 2)));
refr        = refr(idx-63:idx+8192-64, :);
newRef      = (refl(:,1) + refr(:,2))/2;

if fs_L ~= fs
  newRef  = resample(newRef, fs, fs_L);
end
newRef  = newRef(1:N, :);
REFL    = fft([newRef; zeros(N,1)]);
[~, tempIdx]  = min( abs( freq-refFreq ) );

if freq(tempIdx,1) > refFreq
  val1      = abs(REFL( tempIdx-1, 1));
  val2      = abs(REFL( tempIdx, 1));
  refVal    = ( (freq(tempIdx,1) - refFreq)*val1 + (refFreq-freq(tempIdx-1,1))*val2 ) / (freq(tempIdx,1)-freq(tempIdx-1,1));
elseif freq(tempIdx, 1) < refFreq
  val1      = abs(REFL( tempIdx, 1));
  val2      = abs(REFL( tempIdx+1, 1));
  refVal    = ( (freq(tempIdx+1, 1) - refFreq)*val1 + (refFreq-freq(tempIdx,1))*val2 ) / (freq(tempIdx+1,1)-freq(tempIdx,1));
else
  refVal    = abs( REFL(tempIdx, 1));
end

REFL = abs(REFL) ./ refVal;
[upperLim] = find(REFL(1:N+1, 1)>1, 1, 'last');
REFL(upperLim+1:N+1, 1) = 1;
INVRESP = 1./REFL(1:N+1, 1);
INVRESP(1:round(tempIdx/2), 1) = 1;

for jj = 1 : length(azim)
  sHFl      = zeros(N*2, 1);
  sHFr      = zeros(N*2, 1);
  sHRl      = zeros(N*2, 1);
  sHRr      = zeros(N*2, 1);

  nn    = 0;
  itd1  = 0;
  itd2  = 0;
  for ii = 1 : length(D)
    if D(ii).isdir == 1 & D(ii).name(1) ~= '.' & D(ii).name(1:2) == 'su'
      nn = nn+1;
      fileName  = sprintf('%s', folder, D(ii).name, '/hrir_final.mat');
      load(fileName);
      
      hFl   = squeeze(hrir_l(azim(jj), elev(1), :));
      hFr   = squeeze(hrir_r(azim(jj), elev(1), :));
      if fs == 48000
        hFl   = resample(hFl, 48000, 44100);
        hFr   = resample(hFr, 48000, 44100);
      end
      
      [c1, lags1]   = xcorr(hFl, hFr, 'coeff');
      s1            = lags1( abs(c1) == max(abs(c1)) );
      itd1          = itd1 + s1;
      
      
      hRl   = squeeze( hrir_l(azim(jj), elev(2), :));
      hRr   = squeeze( hrir_r(azim(jj), elev(2), :));
      if fs == 48000
        hRl   = resample(hRl, 48000, 44100);
        hRr   = resample(hRr, 48000, 44100);
      end
      
      [c2, lags2]   = xcorr(hRl, hRr, 'coeff');
      s2            = lags2( abs(c2) == max(abs(c2)) );
      itd2          = itd2 + s2;
      
      HFl   = fft( hFl, N*2);
      HFr   = fft( hFr, N*2);
      HRl   = fft( hRl, N*2);
      HRr   = fft( hRr, N*2);
      
      sHFl  = sHFl + abs(HFl);
      sHFr  = sHFr + abs(HFr);
      sHRl  = sHRl + abs(HRl);
      sHRr  = sHRr + abs(HRr);

    end
  end
  sHFl      = sHFl / nn;
  sHFr      = sHFr / nn;
  sHRl      = sHRl / nn;
  sHRr      = sHRr / nn;
  
  itd1      = round(itd1/nn);
  itd2      = round(itd2/nn);
  
  if jj <= 3
    refValF   = sHFl(freqIdx, 1);
    refValR   = sHRl(freqIdx, 1);
    ildF  = sHFr(1:N+1,1) ./ sHFl(1:N+1,1);
    ildR  = sHRr(1:N+1,1) ./ sHRl(1:N+1,1);
  else
    refValF   = sHFr(freqIdx,1);
    refValR   = sHRr(freqIdx,1);
    ildF  = sHFl(1:N+1,1) ./ sHFr(1:N+1,1);
    ildR  = sHRl(1:N+1,1) ./ sHRr(1:N+1,1);
  end
  
  sHFl      = sHFl / refValF;
  sHFr      = sHFr / refValF;
  
  sHRl      = sHRl / refValR;
  sHRr      = sHRr / refValR;
  
  exponent  = 1/2;
  FRONT   = zeros(N+1,2);
  REAR    = zeros(N+1,2);
  if jj <= 3
    idxF  = find( sHFl(1:N, 1)>1);
    idxR  = find( sHRl(1:N, 1)>1);
    
    FRONT(:,1)        = sHFl(1:N+1,1);
    FRONT(idxF, 1)    = sHFl(idxF,1).^exponent;
    FRONT(1:N+1,1)    = FRONT(1:N+1,1) .* INVRESP;
    FRONT(1:N+1,2)    = FRONT(1:N+1,1) .* ildF;
    
    REAR(:,1)       = sHRl(1:N+1,1);
    REAR(idxR, 1)   = sHRl(idxR, 1).^exponent;
    REAR(1:N+1,1)   = REAR(1:N+1,1) .* INVRESP;
    REAR(1:N+1,2)   = REAR(1:N+1,1) .* ildR;

  else
    idxF  = find( sHFr(1:N, 1)>1);
    idxR  = find( sHRr(1:N, 1)>1);
    
    FRONT(:,2)        = sHFr(1:N+1,1);
    FRONT(idxF, 2)    = sHFr(idxF,1).^exponent;
    FRONT(1:N+1,2)      = FRONT(1:N+1,2) .* INVRESP;
    FRONT(1:N+1,1)      = FRONT(1:N+1,2) .* ildF;
    
    REAR(:,2)         = sHRr(1:N+1,1);
    REAR(idxR, 2)     = sHRr(idxR,1).^exponent;
    REAR(1:N+1,2)       = REAR(1:N+1,2) .* INVRESP;
    REAR(1:N+1,1)       = REAR(1:N+1,2) .* ildR;
  end
  FRONT(N+2:N*2,:)     = FRONT(N:-1:2, :);
  REAR(N+2:N*2,:)      = REAR(N:-1:2, :);

%  % optional REAR treat
%  originNrg   = sqrt( sum( (sHRl(1:N+1,1).*freqGain).^2 ) + sum( (sHRr(1:N+1,1).*freqGain).^2 ) );
%  processedNrg = sqrt( sum( REAR(1:N+1,1).^2 ) + sum( REAR(1:N+1,2).^2 ) );
%  processingGain = originNrg / processedNrg;
%  REAR  = REAR * processingGain;
  
  FRONT   = fftshift(FRONT);
  minPhaseF   = unwrap(-imag(hilbert(log(abs(FRONT)))));
  REAR    = fftshift(REAR);
  minPhaseB   = unwrap(-imag(hilbert(log(abs(REAR)))));
  
  FRONT   = FRONT .* exp(1i*minPhaseF);
  REAR    = REAR .* exp(1i*minPhaseB);
  
  FRONT   = ifftshift(FRONT);
  REAR    = ifftshift(REAR);
    
  front   = real( ifft( FRONT ) );
  rear    = real( ifft( REAR ) );
  
  if jj == 1
    [B3, A3, ~]   = peakFilter( 9000, -1.5, 4, fs, N+1);
    front   = filter(B3, A3, filter(B1, A1, filter(B2, A2, front)));
    rear    = filter(B3, A3, filter(B1, A1, filter(B2, A2, rear)));
    
  elseif jj == 2
    [B3, A3, ~]   = peakFilter( 9000, -2.5, 4, fs, N+1);
    front   = filter(B1, A1, filter(B2, A2, front));
    front(:,1)  = filter(B3, A3, front(:,1));
    rear    = filter(B1, A1, filter(B2, A2, rear));
    rear(:,1)   = filter(B3, A3, rear(:,1));
    
  elseif jj == 3
    [B3, A3, ~]   = peakFilter( 9000, -4, 4, fs, N+1);
    front     = filter(B1, A1, filter(B2, A2, front));
    front(:,1)  = filter(B3, A3, front(:,1));
    rear    = filter(B1, A1, filter(B2, A2, rear));
    rear(:,1)   = filter(B3, A3, rear(:,1));
    
  elseif jj == 4
    [B3, A3, ~]   = peakFilter( 9000, -2.5, 4, fs, N+1);
    front   = filter(B1, A1, filter(B2, A2, front));
    front(:,2)  = filter(B3, A3, front(:,2));
    rear    = filter(B1, A1, filter(B2, A2, rear));
    rear(:,2)   = filter(B3, A3, rear(:,2));
    
  elseif jj == 5
    [B3, A3, ~]   = peakFilter( 9000, -4, 4, fs, N+1);
    front   = filter(B1, A1, filter(B2, A2, front));
    front(:,2)  = filter(B3, A3, front(:,2));
    rear    = filter(B1, A1, filter(B2, A2, rear));
    rear(:,2)     = filter(B3, A3, rear(:,2));
  end

%   if jj == 3 || jj == 5
%     front   = filter(B1, A1, filter(B3, A3, filter(B2, A2, front)));
%     frontNrg  = sqrt( sum(front(:,1).^2) + sum(front(:,2).^2) );
%     rear    = filter(B1, A1, filter(B3, A3, filter(B2, A2, rear)));
%     rearNrg   = sqrt( sum(rear(:,1).^2) + sum(rear(:,2).^2) );
% %     rear      = rear*frontNrg / rearNrg;
%   else
%     front   = filter(B1, A1, filter(B2, A2, front));
%     rear    = filter(B1, A1, filter(B2, A2, rear));
%   end
%   if jj == 1
%     front   = filter(B3, A3, filter(B1, A1, filter(B2, A2, front)));
%     rear    = filter(B3, A3, filter(B1, A1, filter(B2, A2, rear)));
%   elseif jj == 2 || jj == 3
%     front(:,1)  = filter(B3, A3, filter(B1, A1, filter(B2, A2, front(:,1))));
%     front(:,2)  = filter(B1, A1, filter(B2, A2, front(:,2)));
%     rear(:,1)   = filter(B3, A3, filter(B1, A1, filter(B2, A2, rear(:,1))));
%     rear(:,2)   = filter(B1, A1, filter(B2, A2, rear(:,2)));
%   else
%     front(:,2)  = filter(B3, A3, filter(B1, A1, filter(B2, A2, front(:,2))));
%     front(:,1)  = filter(B1, A1, filter(B2, A2, front(:,1)));
%     rear(:,2)   = filter(B3, A3, filter(B1, A1, filter(B2, A2, rear(:,2))));
%     rear(:,1)   = filter(B1, A1, filter(B2, A2, rear(:,1)));
%   end

  if fs == 48000
    default   = 16;
  else
    default   = 15;
  end
  
  if itd1 < 0
    front(:,1)  = circshift(front(:,1), default);
    front(:,2)  = circshift(front(:,2), default + abs(itd1));
  elseif itd1 > 0
    front(:,1)  = circshift(front(:,1), default + abs(itd1));
    front(:,2)  = circshift(front(:,2), default);
  elseif itd1 == 0
    front       = circshift(front, default);
  end
  
  if itd2 < 0
    rear(:,1)  = circshift(rear(:,1), default);
    rear(:,2)  = circshift(rear(:,2), default + abs(itd2));
  elseif itd2 > 0
    rear(:,1)  = circshift(rear(:,1), default + abs(itd2));
    rear(:,2)  = circshift(rear(:,2), default);
  elseif itd2 == 0
    rear       = circshift(rear, default);
  end
  frontName   = sprintf('%s', outFolder, 'hrir_', fName{jj}, '_', num2str(round(fs/1000)), 'k.wav');
  rearName    = sprintf('%s', outFolder, 'hrir_', rName{jj}, '_', num2str(round(fs/1000)), 'k.wav');
  audiowrite(frontName, front(1:192, :)*0.7, fs);
  audiowrite(rearName, rear(1:192, :)*0.7, fs);
  
  FRONT1   = abs(fft(front(1:192, :), 1024));
  
  figure();
  semilogx(freq, 20*log10(abs([sHFl(1:N+1) sHFr(1:N+1)])));
  hold on;
  semilogx(freq, 20*log10(FRONT1(1:N+1, :)), '--', 'linewidth', 1.5);
  grid on;
%   audiowrite(frontName, front*0.7, fs);
%   audiowrite(rearName, rear*0.7, fs);
end
