clc;
clear all;
close all;

addpath 'CIPIC_hrtf_database/special_kemar_hrir/kemar_frontal';
addpath 'CIPIC_hrtf_database/special_kemar_hrir/kemar_horizontal';

%% Params
load('large_pinna_frontal.mat');

kMinEle     = -45;
kMaxEle     = 230.6250;
kIntEle     = 2.8125;
ele         = kMinEle : kIntEle : kMaxEle;
kZeroIdx    = find(ele==0);

kFs         = 44100;
kNumSamples = length(left);
kNumDir     = length(left(1, :));
t           = 0 : 1/kFs : (kNumSamples - 1) * 1/kFs;

kFmin       = 100;
kFmax       = kFs/2;
kNFFT       = 1024;
kQ          = Inf;
logScale    = 1;

%% Time-Domain Plot 
fig1 = figure(1); fig1.Position = [30 130 600 600];
targetIdx = 1;

subplot(211);
plot(t, left(:, targetIdx), 'k');
axis([0 (kNumSamples - 1) * 1/kFs -2.5 2.5]);
grid on;

subplot(212);
plot(t, right(:, targetIdx), 'k');
axis([0 (kNumSamples - 1) * 1/kFs -2.5 2.5]);
grid on;

%% Freq-Domain Data
% Zero
[dBZeroL, sampfreqns] = freq_resp(left(:, kZeroIdx), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
[dBZeroR, sampfreqns] = freq_resp(right(:, kZeroIdx), kFmin, kFmax, kQ, logScale, kNFFT, kFs);

for i = 1:kNumDir
    [dBL(:, i), sampfreqns] = freq_resp(left(:, i), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    [dBR(:, i), sampfreqns] = freq_resp(right(:, i), kFmin, kFmax, kQ, logScale, kNFFT, kFs);
    
    dBLNormal(:, i) = dBL(:, i) ./ dBZeroL;
    dBRNormal(:, i) = dBR(:, i) ./ dBZeroR;
end;

clims = [-20 20];
ticks = [0.125 0.25 0.5 1.0 2.0 4.0 8.0 16.0];

fig2 = figure(2); fig2.Position = [30 100 1200 600];
subplot(1,2,1);
imagesc(ele, sampfreqns*1E-3, dBL, clims); colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
subplot(1,2,2);
imagesc(ele, sampfreqns*1E-3, dBR, clims); colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');

fig3 = figure(3); fig3.Position = [100 100 1200 600];
subplot(1,2,1);
imagesc(ele, sampfreqns*1E-3, dBLNormal, clims); colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');
subplot(1,2,2);
imagesc(ele, sampfreqns*1E-3, dBRNormal, clims); colorbar;
set(gca, 'YTick', ticks, 'YScale', 'log', 'YDir', 'normal');




